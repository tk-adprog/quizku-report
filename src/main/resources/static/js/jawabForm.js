$(document).ready(function(){

    var map = {}
    map["token"] = sessionStorage.getItem("token");

    map = JSON.stringify(map);

    $.ajax({
        type:"POST",
        url: window.location.pathname + '/durationLeft',
        header : { "Authorization": "Bearer " + sessionStorage.getItem("token")},
        contentType: "application/json; charset=utf-8",
        data: map,
        success:function(data){
            //console.log(data);
            $("#timerr").html(data);
            var myVar;
            var timer = data;
            var countDownSeconds;
            startTime();
            function startTime(){
                myVar = setInterval(start, 60000);
                $("#timerr").html(timer);
                countDownSeconds = timer;
            }

            function start(){
                countDownSeconds--;
                $("#timerr").html(countDownSeconds);
                if (countDownSeconds == 0){
                    $( "#submit" ).trigger( "click" );
                    $("#timerr").html("0");
                }
            }
        }
    })

    $.ajax({
        type: "POST",
        url: window.location.pathname,
        header : { "Authorization": "Bearer " + sessionStorage.getItem("token")},
        //dataType: 'json',
        contentType: "application/json; charset=utf-8",
        data: map,
        success: function(data) {
            $("#title").append($(
            '<h1 class="mb-4">' + data.title + '</h1>'
            ));
            jQuery.each(data.questionList, function (i, question) {
                //console.log(question);
                //console.log(question.questionId.number);
                if (question.option.length == 0) {
                   var string = '<div class= "kelompok"> <div class="question essay">' +
                   '<h1>' + question.question + '</h1>' +
                   '<div class="number" name="'+ question.questionId.number +'"> </div>' +
                   '<div class="tipe" name= "essay"> </div> </div> <br>' +
                   '<div class="answer"> <input type="text" class="form-control selectedEssay" name="answerEssay" placeholder="Isi disini"> </div> </div>'
                   $("#inject1").append($(
                       string + '<br> <br>'
                   ))
                }
                else {
                   var string = '<div class= "kelompok"> <div class="question pilgan" name =' + question.questionId.number +'">' +
                    '<h1>' + question.question + '</h1>' +
                     '<div class="number" name=" '+ question.questionId.number +'"> </div>' +
                     '<div class="tipe" name= "pilgan"> </div> </div> <br>' +
                     '<div class="answer">'
                     jQuery.each(question.option, function (j, option) {
                        string += '<div class="option btn btn-light px-3 py-2 primary-btn float-center" name="answer">' + option + '</div> <br> <br>';
                     })
                     string += '</div>'
                     $("#inject1").append($(
                         string + '<br>'
                      ))
                }
            })

            //console.log("value timer")
            //console.log(timer);

        }
    });
    $(document).on('click', ".option", function () {
        var parent = $(this).parent();
        
        $(parent).children('div').each(function () {
            $(this).removeClass('selected');
        });
        
        $(this).addClass('selected');
    });

    $(document).on('click', "#submit", function () {
        var json = {}
        var list = []
        var kelompok = $("#inject1").find(".kelompok");
        //console.log(kelompok);

        jQuery.each(kelompok, function (k, question) {
            //console.log(question);
            var valueList = {}
            var questionElement = $(question).find(".question");
            var questionStringElement = $(questionElement).find('h1');
            valueList["questionString"] = $(questionStringElement).text();

            var nomorPertanyaan = $(questionElement).find(".number");
            valueList["number"] = $(nomorPertanyaan).attr('name');

            var tipePertanyaan = $(questionElement).find(".tipe");
            valueList["tipe"] = $(tipePertanyaan).attr('name');

            if ($(tipePertanyaan).attr('name') == "essay") {
                var jawaban = $(question).find(".selectedEssay");
                valueList["answer"] = $(jawaban).val();
                
            }
            else if ($(tipePertanyaan).attr('name') == "pilgan") {
                var jawaban = $(question).find(".selected");
                valueList["answer"] = $(jawaban).text();
            }

            list.push(valueList);
            
        });

        var map = {}
        map["json"] = list;
        map["token"] = sessionStorage.getItem("token");

        map = JSON.stringify(map);
        //console.log(map);

        $.ajax({
            type: 'POST',
            header : { "Authorization": "Bearer " + sessionStorage.getItem("token")},
            url: window.location.pathname + '/submit',
            data: map,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                //console.log("data")
                //console.log(data.quizReportId);
                window.location = `/profile/${data.quizReportId.quizId}`
            },
            fail: function () {
                alert('ERROR!');
            }
        });
    });


});
