$( document ).ready(function() {
    console.log( "ready!" );

    var json = {}
    console.log("Quiz Id as Ajax Parameter");
    var quizId = $('#idQuizInput').val();
    console.log(quizId);

    json['token'] = sessionStorage.getItem('token');
    json = JSON.stringify(json);

    $.ajax({
        type : "POST",
        crossDomain : true,
        beforeSend: function(xhr){
            xhr.setRequestHeader("Authorization", "Bearer " + sessionStorage.getItem("token"))
        },
        // header : { "Authorization": "Bearer " + sessionStorage.getItem("token")},
        contentType: "application/json; charset=utf-8",
        // dataType : 'json',
        data : json,
        url : window.location.pathname + "/gradeNya",
        success: function(data){
            console.log(data);
            $("#score").append($('<h2 class="btn btn-light px-3 py-2 primary-btn float-right" id="score">\n' +
                'Score : ' + data +
                '</h2>'))
        }
    })
    $.ajax({
        type : "POST",
        crossDomain : true,
        beforeSend: function(xhr){
            xhr.setRequestHeader("Authorization", "Bearer " + sessionStorage.getItem("token"))
        },
        url : window.location.pathname + "/questionReport",
        // header : { "Authorization": "Bearer " + sessionStorage.getItem("token")},
        contentType: "application/json; charset=utf-8",
        dataType : 'json',
        data : json,
        success: function(data){
            console.log(data);
            console.log(data.length);
            for (i = 0; i < data.length; i++) {
                var nomor = i + 1;
                var question = data[i].questionString;
                var rightAnswer = data[i].rightAnswer;
                var isTrue = data[i].true;
                var userAnswer = data[i].userAnswer;
                console.log(question);
                console.log(rightAnswer);
                console.log(isTrue);
                console.log(userAnswer);
                if (isTrue == true){
                    $("#kotak").append(
                        '    <div class="box-header">\n' +
                        '      <h3>Question ' + nomor + '<span id = "tf" style="font-size: 0.8em; background: linear-gradient(90deg, #1F8C8C 0%, #24BFA3 111.76%); color: #FFFFFF; padding-left: 1em; padding-right: 1em; border-radius: 2em; margin-left: 1em" >True </span></h3>\n' +
                        '    </div>' +
                        '<div class="question">\n' +
                        question +
                        '      <br><br><span style="font-size: 0.8em;"> Your Answer : </span><div class="answer" style="background: linear-gradient(270.17deg, #B0D9CD 29.34%, #80BFB4 138.13%); border-radius: 1em;">\n' +
                        '        <p style="font-size: 1em; padding-left: 1em;">' + userAnswer + '</p>\n' +
                        '      </div><br>\n' +
                        '    </div><br><br>')
                } else {
                    $("#kotak").append(
                        '    <div class="box-header">\n' +
                        '      <h3>Question ' + nomor + '<span id = "tf" style="font-size: 0.8em; background: linear-gradient(90deg, #8C1F1F 0%, #BF2424 111.76%); color: #FFFFFF; padding-left: 1em; padding-right: 1em; border-radius: 2em; margin-left: 1em" >False </span></h3>\n' +
                        '    </div>' +
                        '<div class="question">\n' +
                        question +
                        '      <br><br><span style="font-size: 0.8em;"> Your Answer : </span><div class="answer" style="background: linear-gradient(270.17deg, #D9B0B0 29.34%, #BF8080 138.13%); border-radius: 1em;">\n' +
                        '        <p style="font-size: 1em; padding-left: 1em;">' + userAnswer + '</p>\n' +
                        '      </div>\n' +
                        '      <span style="font-size: 0.8em;"> Right Answer : </span><div class="answer" style="background: linear-gradient(90deg, #1F8C8C 0%, #24BFA3 111.76%); border-radius: 1em;">\n' +
                        '        <p style="font-size: 1em; padding-left: 1em;">' + rightAnswer + '</p>\n' +
                        '      </div><br>\n' +
                        '    </div><br><br>')
                }
            }
        }
    })
});