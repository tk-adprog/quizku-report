$(document).ready(function () {

    $(document).on('click', "#finish", function () {
        var json = {}
        var title = $("#form-quiz").find("#title").val();
        if (title.length == 0) {
            alert('Title is empty!')
            return false;
        }
        var imageUrl = $("#form-quiz").find("#imageUrl").val();
        if (imageUrl.length == 0) {
            alert('Image url is empty!')
            return false;
        }
        var duration = $("#form-quiz").find("#duration").val();
        if (duration < 10 || duration > 120) {
            alert('Duration must between 10 and 120 minutes!')
            return false;
        }
        if (duration.length == 0) {
            alert('Duration is empty!')
            return false;
        }
        var description = $("#form-quiz").find("#desc").val();
        if (description.length == 0) {
            alert('Description is empty!')
            return false;
        }
        var tag = $("#form-quiz").find("#tag").val();
        if (tag.length == 0) {
            alert('Tag is empty!')
            return false;
        }
        json["title"] = title;
        json["description"] = description;
        json["duration"] = duration;
        json["tag"] = tag;
        json["imageUrl"] = imageUrl;
        console.log(JSON.stringify(json));

        var questionList = []
        var questionElement = $("#form-question").find(".type");
        if (questionElement.length == 0) {
            alert("No Question!");
            return false;
        }
        var isLoopSuccess = true;
        // Loop every form
        jQuery.each(questionElement, function (i, iVal) {
            if (isLoopSuccess == false) {
                return false;
            }
            questionJson = {}
            // Get question type
            var type = $(iVal).attr("value")
            questionJson["type"] = type;

            var eachQuestionElement = $(iVal).children();
            var questionInputElement = $(eachQuestionElement[0]).children();
            var choiceInputElement = $(eachQuestionElement[1]).children();
            // Get question string
            var question = $(questionInputElement[0]).val();
            if (question.length == 0) {
                alert('Question is empty!')
                isLoopSuccess = false
            }
            questionJson["question"] = question;
            var optionArray = [];

            // Get choice string
            jQuery.each(choiceInputElement, function (j, jVal) {
                if (isLoopSuccess == false) {
                    return false;
                }
                if ($(jVal).val().length == 0) {
                    alert('Answer is empty!')
                    isLoopSuccess = false;
                }
                optionArray.push($(jVal).val());
            })
            questionJson["options"] = optionArray;
            console.log(questionJson);
            questionList.push(questionJson);
        });
        if (!isLoopSuccess) {
            return false;
        }
        json["questions"] = questionList;
        json["token"] = sessionStorage.getItem("token");
        console.log(json);
        json = JSON.stringify(json)
        console.log(json);
        $.ajax({
            type: 'POST',
            header : { "Authorization": "Bearer " + sessionStorage.getItem("token")},
            url: "/quiz/register",
            header : { "Authorization": "Bearer " + sessionStorage.getItem("token")},
            data: json,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                window.location = '/quiz'
            },
            fail: function () {
                alert('ERROR!');
            }
        });
    });

    $(document).on('click', "#next", function () {
        var quizDiv = $(document).find("#quiz-div")
        var questionDiv = $(document).find("#question-div")
        $(quizDiv).addClass("d-none")
        $(questionDiv).removeClass("d-none")
    });

    $(document).on('click', "#back", function () {
        var quizDiv = $(document).find("#quiz-div")
        var questionDiv = $(document).find("#question-div")
        $(questionDiv).addClass("d-none")
        $(quizDiv).removeClass("d-none")
    });

    $(".choiceButton").click(function () {
        console.log("pg");
        $("#listQuestion").append($(`
            <li>
                <div class="form-group type" name="type" value="Choice">
                    <div>
                        <input type="text" class="form-control question" name="question" placeholder="Question">
                    </div>

                    <div class="answer">
                        <input type="text" class="form-control" name="a" id="a" placeholder="True Option">
                        <input type="text" class="form-control" name="b" id="b" placeholder="False Option">
                        <input type="text" class="form-control" name="c" id="c" placeholder="False Option">
                        <input type="text" class="form-control" name="d" id="d" placeholder="False Option">
                    </div>
                </div>
                <div class="deleteButton">
                    delete
                </div>
            </li>
        `));
    });

    $(".essayButton").click(function () {
        console.log("essay");
        $("#listQuestion").append($(`
            <li>
                <div class="form-group type" name="type" value="Essay">
                    <div>
                        <input type="text" class="form-control question" name="question" placeholder="Question">
                    </div>
                        <div class="question">
                        <input type="text" class="form-control" name="answer" id="answer" placeholder="Answer">
                        </div>
                    </div>
                <div class="deleteButton">
                    delete
                </div>
            </li>

        `));
    });

    $(document).on('click', ".deleteButton", function () {
        console.log("delete");
        $(this).parent().remove();
    });
});