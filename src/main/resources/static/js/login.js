
$(document).ready(function () {
    $("#submit").click(function (e) {
        $.ajax({
            type: 'POST',
            header : { "Authorization": "Bearer " + sessionStorage.getItem("token")},
            data: JSON.stringify({username: $("#username").val(), password: $("#password").val()}),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: "/authenticate"
        }).done(function (response){
            sessionStorage.setItem("token",response.token);
            sessionStorage.setItem("Authorization", 'Bearer ' + sessionStorage.getItem("token"))

            window.location.href = "/";
        })

    });
});

// function CallAPI(token) {

// }

// $("#submit").click(function (e) {
//     $.ajax({
//             type: "POST",
//             url: "@Url.Action("Index")",
//             data: {username: $("#username").val(), password: $("#password").val()},
//             dataType: "text",
//             success: function (result) {
//                 if (result != "Error")
//                     CallAPI(result);
//             }
//         },
//         error: function (req, status, error) {
//         alert(error);
//     }
// });
// });

// function ShowData(reservations) {
//     $.ajax({
//         type: "POST",
//         contentType: "application/json",
//         url: "@Url.Action("Reservation")",
//         data: JSON.stringify(reservations),
//         success: function (result) {
//             $("#reservations").html(result)
//         },
//         error: function (req, status, error) {
//             alert(error);
//         }
//     });
// }
// });