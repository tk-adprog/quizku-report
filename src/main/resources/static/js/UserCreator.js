$(document).ready(function () {

    $(document).on('click', "#create", function () {
        var json = {}
        var username = $("#form-user").find("#username").val();
        if (username.length == 0) {
            alert('Username is empty!')
            return false;
        }

        var fullname = $("#form-user").find("#fullname").val();
        if (fullname.length == 0) {
            alert('Fullname is empty!')
            return false;
        }

        var email = $("#form-user").find("#email").val();
        if (email.length == 0) {
            alert('Email is empty!')
            return false;
        } else if (!ValidateEmail(email)){
            alert('Invalid email address!');
        }

        var birthdate = $("#form-user").find("#birthdate").val();
        if (birthdate.length < 10) {
            alert('Invalid date format!')
            return false;
        }

        var imageUrl = $("#form-user").find("#imageUrl").val();
        if (imageUrl.length == 0) {
            alert('Image url is empty!')
            return false;
        }

        var password = $("#form-user").find("#password").val();
        if (password.length == 0) {
            alert('Password is empty!')
            return false;
        }


        json["username"] = username;
        json["fullname"] = fullname;
        json["email"] = email;
        json["birthdate"] = birthdate;
        json["imageUrl"] = imageUrl;
        json['password'] = password;
        json = JSON.stringify(json);
        console.log(json);
        $.ajax({
            type: 'POST',
            header : { "Authorization": "Bearer " + sessionStorage.getItem("token")},
            url: "/signup/register",
            data: json,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                window.location = '/signup'
            },
            fail: function () {
                alert('ERROR!');
            }
        });
    });

    function ValidateEmail(input) {
        const validRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        if (input.match(validRegex)) {
            return true;

        } else {
            return false;
        }

    }
})