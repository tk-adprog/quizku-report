$(document).ready(function(){
    var json = {}
    console.log("Quiz Id as Ajax Parameter");
    var quizId = $('#idQuizInput').val();
    console.log(quizId);

    json['idQuiz'] = quizId;
    json['token'] = sessionStorage.getItem("token");
    json = JSON.stringify(json);

    $.ajax({
        type: 'POST',
        header: { "Authorization": "Bearer " + sessionStorage.getItem("token")},
        contentType: "application/json; charset=utf-8",
        data: json,
        url: '/profile/' + quizId,
        success: function(data){
            console.log(data)
            let dataQuiz = '';
            dataQuiz+= `<h5 class="card-title">Tittle : ${data[2]}</h5>  ` +
                `<h5 class="card-title">Questions : ${data[3]}</h5> ` +
                `<h5 class="card-title">Duration: ${data[4]} m</h5> ` +
                `<h5 class="card-text">${data[5]}</h5> `
                if(data[1] == 0){
                    dataQuiz+= `<a href="/jawab/${quizId}" class="btn btn-primary d-flex justify-content-center hijauGradient" style="border-radius: 1vh">Attempt Now</a>`
                }else{
                    dataQuiz+= `<a href="/summary/${quizId}/${data[0]}" class="btn btn-primary d-flex justify-content-center hijauGradient" style="border-radius: 1vh">See Review</a>`
                }
            $('#dataQuiz').html(dataQuiz);
        }
    })

    $.ajax({
        type:'GET',
        header : { "Authorization": "Bearer " + sessionStorage.getItem("token")},
        url:'/profile/leaderboard/' + quizId,
        success: function(data){
            let result = '';
            if(data.length > 0){
                result += '<div class="row leaderboardTitle"> ' +
                    '<div class="col-sm">No</div> ' +
                    '<div class="col-sm text-center">Nama Peserta</div> ' +
                    '<div class="col-sm text-center">Score</div> ' +
                    '</div>'
                console.log(data);
                for(let i = 0; i < data.length; i++){
                    counter = i+1
                    result += `<div class="row hijauGradient bgJudul leaderboardList"> ` +
                        `<div class="col-sm">${counter}</div> ` +
                        `<div class="col-sm text-center">${data[i].quizReportId.owner}</div>  ` +
                        `<div class="col-sm text-center">${data[i].grade}</div> ` +
                        `</div>`;

                }
            }else{
             result += '<div class="d-flex justify-content-center">There is no data for this Quiz</div>'
            }
            $('#resultLeaderboard').html(result);
        }
    })

    $.ajax({
        type:"GET",
        header : { "Authorization": "Bearer " + sessionStorage.getItem("token")},
        url:'/profile/statistics/' + quizId,
        success: function(data){
            $('#avgScore').html(data[0]);
            $('#avgDuration').html(data[2]);
            $('#ttlAttempt').html(data[1]);
        }
    })

})

