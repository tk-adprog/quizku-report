package com.tk.quizku.app.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tk.quizku.app.core.util.RestService;
import com.tk.quizku.app.model.Quiz;
import com.tk.quizku.app.model.QuizReport;
import com.tk.quizku.app.repository.QuizReportRepository;
import com.tk.quizku.auth.config.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(path = "/report")
@CrossOrigin
public class QuizReportController {
    @Autowired
    JwtTokenUtil jwtTokenUtil;

    @Autowired
    QuizReportRepository quizReportRepository;

    @Autowired
    RestService restService;

    @PostMapping(path = "/getQuizAnswered",produces = {"application/json"})
    @ResponseBody
    @CrossOrigin
    public ResponseEntity getStatistics(@RequestBody Map<String, Object> json)
            throws JsonProcessingException {
        String token = json.get("token").toString();
        String username = jwtTokenUtil.getUsernameFromToken(token);
        List<QuizReport> quizReportList = quizReportRepository.findByQuizReportIdOwner(username);
        List<Quiz> quizList = new ArrayList<>();
        for(QuizReport element:quizReportList){
            String quizJson = restService.getPostsPlainjson(String.format("https://quizku-quiz.herokuapp.com/quiz/%s", element.getQuizReportId().getQuizId()), token);
            //String quizJson = restService.getPostsPlainjson(String.format("http://localhost:8082/quiz/%s", element.getQuizReportId().getQuizId()), token);
            ObjectMapper objectMapper = new ObjectMapper();
            Quiz quiz = objectMapper.readValue(quizJson, Quiz.class);
            quizList.add(quiz);
        }
        return ResponseEntity.ok(quizList);
    }
}
