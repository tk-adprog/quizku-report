package com.tk.quizku.app.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.tk.quizku.app.model.QuestionReport;
import com.tk.quizku.app.service.SummaryService;
import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/summary")
@CrossOrigin
public class SummaryController {

    @Autowired
    private SummaryService summaryService;

    @GetMapping("/{quizId}")
    @CrossOrigin
    public String getSummaryQuizPage(Model model, @PathVariable(value = "quizId") int quizId) {
        model.addAttribute("quizId", quizId);
        return "summaryQuiz";
    }

    @PostMapping(path = "/{quizId}/gradeNya", produces = {"application/json"})
    @ResponseBody
    @CrossOrigin
    public ResponseEntity getGradeQuizNya(
            @RequestBody Map<String, Object>
                    json, @PathVariable(
                    value = "quizId") int id)
            throws JsonProcessingException {
        int grade = summaryService.getGradeQuiz(id, json);
        return ResponseEntity.ok(grade);
    }

    @PostMapping(path = "/{quizId}/questionReport", produces = {"application/json"})
    @ResponseBody
    @CrossOrigin
    public ResponseEntity getQuestionReport(
            @RequestBody Map<String, Object> json, @PathVariable(
                    value = "quizId") int quizId)
            throws JsonProcessingException {
        List<QuestionReport> questionReportNya = summaryService
                .getQuestionReportList(quizId, json);
        return ResponseEntity.ok(questionReportNya);
    }
}
