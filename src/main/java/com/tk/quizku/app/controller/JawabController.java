package com.tk.quizku.app.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tk.quizku.app.core.util.RestService;
import com.tk.quizku.app.model.Quiz;
import com.tk.quizku.app.model.QuizReport;
import com.tk.quizku.app.repository.QuizReportRepository;
import com.tk.quizku.app.service.QuizReportService;
import com.tk.quizku.auth.config.JwtTokenUtil;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/jawab")
@CrossOrigin
public class JawabController {

    @Autowired
    private QuizReportService quizReportService;

    @Autowired
    private QuizReportRepository quizReportRepository;

    @Autowired
    private RestService restService;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    
    @GetMapping(path = "/{quizId}")
    @CrossOrigin
    public String getJawabForm(Model model, @PathVariable(value = "quizId") int quizId) {
        model.addAttribute("idQuiz",quizId);
        return "jawabForm";
    }

    @PostMapping(path = "/{quizId}/durationLeft", produces = {"application/json"})
    @ResponseBody
    @CrossOrigin
    public ResponseEntity postDurationLeft(@PathVariable(value = "quizId") int quizId,
                                           @RequestBody Map<String, Object> json)
            throws JsonProcessingException {
        String token = json.get("token").toString();
        String quizJson = restService.getPostsPlainjson(String.format("https://quizku-quiz.herokuapp.com/quiz/%s", quizId), token);
        //String quizJson = restService.getPostsPlainjson(String.format("http://localhost:8082/quiz/%s", quizId), token);
        ObjectMapper objectMapper = new ObjectMapper();
        Quiz quiz = objectMapper.readValue(quizJson, Quiz.class);
        String username = jwtTokenUtil.getUsernameFromToken(json.get("token").toString());
        QuizReport reportNow = quizReportRepository
                .findByQuizReportIdQuizIdAndQuizReportIdOwner(
                        quizId, username);
        int[] timerDuration = new int[2];
        if (reportNow.getTimeStart() == 0) {
            reportNow.setTimeStart((int)(System.currentTimeMillis() / 1000));
            reportNow.setDurationLeft(quiz.getDuration());
            timerDuration[0] = (quiz.getDuration() / 60) - 1;
            timerDuration[1] = 60;
        } else {
            int timeUsed = ((int)(System.currentTimeMillis() / 1000)) - reportNow.getTimeStart();
            System.out.println("Waktu Digunakan");
            System.out.println(timeUsed);
            int secondUsed = timeUsed % 60;
            int minuteUsed = (timeUsed - secondUsed) / 60;
            System.out.println(secondUsed);
            System.out.println(minuteUsed);
            System.out.println("Quiz Duration");

            if (quiz.getDuration() > timeUsed) {
                reportNow.setDurationLeft(quiz.getDuration() - timeUsed);
                timerDuration[0] = (quiz.getDuration() / 60) - minuteUsed - 1;
                timerDuration[1] = 60 - secondUsed;
            }
        }
        quizReportRepository.save(reportNow);
        return ResponseEntity.ok(timerDuration);
    }

    @PostMapping(path = "/{quizId}", produces = {"application/json"})
    @ResponseBody
    @CrossOrigin
    public ResponseEntity postJawabForm(
            @PathVariable(value = "quizId") int quizId,
            @RequestBody Map<String, Object> json) throws JsonProcessingException {

        Quiz quiz = quizReportService.random(quizId, json);
        return ResponseEntity.ok(quiz);
    }

    @PostMapping(path = "/{quizId}/submit", produces = {"application/json"})
    @ResponseBody
    @CrossOrigin
    public ResponseEntity submitQuiz(
        @PathVariable(value = "quizId") int quizId,
        @RequestBody Map<String, Object> json) throws JsonProcessingException {
        String token = json.get("token").toString();
        String username = jwtTokenUtil.getUsernameFromToken(token);
        json.put("username", username);
        QuizReport quizReport = quizReportService.submit(quizId, json);
        return ResponseEntity.ok(quizReport);
    }

    @GetMapping(path = "/all")
    @CrossOrigin
    public ResponseEntity<Iterable<QuizReport>> getListJawabForm() {
        return ResponseEntity.ok(quizReportService.getAllQuizReport());
    }

    @GetMapping(path = "/all/{quizId}")
    @CrossOrigin
    public ResponseEntity<Iterable<QuizReport>> deleteJawabFormById(
            @PathVariable(value = "quizId") int quizId) {
        quizReportService.deleteQuizReportById(quizId);
        return ResponseEntity.ok(quizReportService.getAllQuizReport());
    }

    @GetMapping(path = "/self/destruct", produces = {"application/json"})
    @CrossOrigin
    public String deleteAll() {
        quizReportRepository.deleteAll();
        return "redirect:/jawab/all";
    }
}
