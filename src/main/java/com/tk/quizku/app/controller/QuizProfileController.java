package com.tk.quizku.app.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tk.quizku.app.core.util.RestService;
import com.tk.quizku.app.model.Quiz;
import com.tk.quizku.app.model.QuizReport;
import com.tk.quizku.app.repository.QuizReportRepository;
import com.tk.quizku.app.service.QuizProfileService;
import com.tk.quizku.auth.config.JwtTokenUtil;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path = "/profile")
@CrossOrigin
public class QuizProfileController {
    @Autowired
    private QuizReportRepository quizReportRepository;

    @Autowired
    private QuizProfileService quizProfileService;

    @Autowired
    private RestService restService;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @GetMapping("/{id}")
    @CrossOrigin
    public String getQuizProfile(Model model, @PathVariable(value = "id")int idQuiz) {
        model.addAttribute("idQuiz",idQuiz);
        return "quizProfile";
    }

    @PostMapping(value = "/{id}", produces = {"application/json"})
    @ResponseBody
    @CrossOrigin
    public ResponseEntity addQuizProfileData(
            @RequestBody Map<String, Object> json, @PathVariable(
                    value = "id")int idQuiz)
            throws JsonProcessingException {
        String[] quizData = new String[7];
        String token = json.get("token").toString();
        int id = idQuiz;
        quizData[0] = jwtTokenUtil.getUsernameFromToken(token);
        String username = jwtTokenUtil.getUsernameFromToken(token);
        quizData[1] = Integer.toString(quizProfileService.updateStatus(id, username, token));
        String quizJson = restService.getPostsPlainjson(String.format("https://quizku-quiz.herokuapp.com/quiz/%s", idQuiz), token);
        //String quizJson = restService.getPostsPlainjson(String.format("http://localhost:8082/quiz/%s", idQuiz), token);
        ObjectMapper objectMapper = new ObjectMapper();
        Quiz quizNow = objectMapper.readValue(quizJson, Quiz.class);
        quizData[2] = quizNow.getTitle();
        quizData[3] = Integer.toString(quizNow.getQuestionList().size());
        quizData[4] = Integer.toString(quizNow.getDuration());
        quizData[5] = quizNow.getDescription();
        quizData[6] = quizNow.getImageUrl();
        return ResponseEntity.ok(quizData);
    }

    @GetMapping(path = "/leaderboard/{id}",produces = {"application/json"})
    @ResponseBody
    @CrossOrigin
    public ResponseEntity getLeaderboard(@PathVariable(value = "id")int id) {
        List<QuizReport> sortedQuiz = quizProfileService.getSortedQuiz(id);
        return ResponseEntity.ok(sortedQuiz);
    }

    @PostMapping(path = "/statistics/{id}",produces = {"application/json"})
    @ResponseBody
    @CrossOrigin
    public ResponseEntity getStatistics(
            @PathVariable(value = "id")int id, @RequestBody Map<String, Object> json)
            throws JsonProcessingException {
        int[] statistics = new int[3];
        String token = json.get("token").toString();
        statistics[0] = quizProfileService.getAverageScore(id);
        statistics[1] = quizProfileService.getTotalAttempt(id);
        statistics[2] = quizProfileService.getAverageTime(id, token);
        return ResponseEntity.ok(statistics);
    }
}
