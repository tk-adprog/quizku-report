package com.tk.quizku.app.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tk.quizku.app.core.util.RestService;
import com.tk.quizku.app.model.Quiz;
import com.tk.quizku.app.model.QuizReport;
import com.tk.quizku.app.model.QuizReportId;
import com.tk.quizku.app.repository.QuizReportRepository;
import com.tk.quizku.app.repository.UserQuizRepository;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class QuizProfileServiceImpl implements QuizProfileService {

    @Autowired
    private QuizReportRepository quizReportRepository;

    @Autowired
    private UserQuizRepository userQuizRepository;

    @Autowired
    private RestService restService;

    @Override
    public List<QuizReport> getSortedQuiz(int idQuiz) {
        List<QuizReport> quizReports = quizReportRepository.
                findByQuizReportIdQuizId(idQuiz);
        List<QuizReport> doneQuiz = new ArrayList<>();
        for (int i = 0; i < quizReports.size(); i++) {
            QuizReport quizReportNow = quizReports.get(i);
            System.out.println(quizReportNow);
            if (quizReportNow.getStatus() == 1) {
                doneQuiz.add(quizReports.get(i));
            }
        }
        Collections.sort(doneQuiz,Collections.reverseOrder());
        return doneQuiz;
    }

    //    @Override
    //    public Quiz getQuizById(int idQuiz) {
    //        return quizRepository.findByIdQuiz(idQuiz);
    //    }

    @Override
    public int getAverageScore(int idQuiz) {
        List<QuizReport> quizReports = quizReportRepository.findByQuizReportIdQuizId(idQuiz);
        int totalScore = 0;
        for (int i = 0; i < quizReports.size(); i++) {
            if (quizReports.get(i).getStatus() == 1) {
                totalScore += quizReports.get(i).getGrade();
            }
        }
        if (quizReports.size() > 0) {
            return totalScore / quizReports.size();
        }
        return 0;

    }

    @Override
    public int getTotalAttempt(int idQuiz) {
        return quizReportRepository.findByQuizReportIdQuizId(idQuiz).size();
    }

    @Override
    public int getAverageTime(int idQuiz, String token) throws JsonProcessingException {
        List<QuizReport> quizReports = quizReportRepository.findByQuizReportIdQuizId(idQuiz);
        String quizJson = restService.getPostsPlainjson(String.format("https://quizku-quiz.herokuapp.com/quiz/%s", idQuiz), token);
        //String quizJson = restService.getPostsPlainjson(String.format("http://localhost:8082/quiz/%s", idQuiz), token);
        ObjectMapper objectMapper = new ObjectMapper();
        Quiz quiz = objectMapper.readValue(quizJson, Quiz.class);
        List<QuizReport> doneQuiz = new ArrayList<>();
        for(int i=0; i<quizReports.size();i++){
            QuizReport quizReportNow = quizReports.get(i);
            System.out.println(quizReportNow);
            if (quizReportNow.getStatus() == 1){
                doneQuiz.add(quizReports.get(i));
            }
        }
        int totalTime = 0;
        for (int i = 0; i < doneQuiz.size(); i++) {
            if(doneQuiz.get(i).getStatus() == 1){
                totalTime += doneQuiz.get(i).getDurationLeft();
            }
        }
        System.out.println(doneQuiz);
        if(doneQuiz.size() > 0){
            int totalSisa = totalTime/quizReports.size();
            int duration = quiz.getDuration();
            System.out.println(totalSisa);
            return (duration - totalSisa);
        }else{
            return 0;
        }
    }

    @Override
    public int updateStatus(int idQuiz, String owner, String token) throws JsonProcessingException {
        QuizReport reportNow = quizReportRepository
                .findByQuizReportIdQuizIdAndQuizReportIdOwner(idQuiz, owner);
        String quizJson = restService.getPostsPlainjson(String.format("https://quizku-quiz.herokuapp.com/quiz/%s", idQuiz), token);
        // String quizJson = restService.getPostsPlainjson(String.format("http://localhost:8082/quiz/%s", idQuiz), token);
        ObjectMapper objectMapper = new ObjectMapper();
        Quiz quiz = objectMapper.readValue(quizJson, Quiz.class);
        int quizDuration = quiz.getDuration();
        if (reportNow == null) {
            //initiate state
            QuizReportId reportIdNow = new QuizReportId(idQuiz, owner);
            reportNow = new QuizReport();
            reportNow.setQuizReportId(reportIdNow);
            reportNow.setDurationLeft(quizDuration);
            quizReportRepository.save(reportNow);
        }
        return reportNow.getStatus();
    }

}