package com.tk.quizku.app.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tk.quizku.app.core.util.RestService;
import com.tk.quizku.app.model.*;
import com.tk.quizku.app.repository.QuestionReportRepository;
import com.tk.quizku.app.repository.QuizReportRepository;
import com.tk.quizku.auth.config.JwtTokenUtil;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class QuizReportServiceImpl implements QuizReportService {

    @Autowired
    QuestionReportRepository questionReportRepository;

    @Autowired
    QuizReportRepository quizReportRepository;

    @Autowired
    RestService restService;

    @Autowired
    JwtTokenUtil jwtTokenUtil;

    @Override
    public QuizReport submit(int idQuiz, Map<String, Object> json)
            throws JsonProcessingException {


        String token = json.get("token").toString();
        String username = json.get("username").toString();

        String quizJson = restService.getPostsPlainjson(String.format("http://quizku-quiz.herokuapp.com/quiz/%s", idQuiz), token);
        ObjectMapper objectMapper = new ObjectMapper();
        Quiz quiz = objectMapper.readValue(quizJson, Quiz.class);

        QuizReport quizReport = quizReportRepository
                .findByQuizReportIdQuizIdAndQuizReportIdOwner(
                        idQuiz, username);

        quizReport.setStatus(1);
        int timeUsed = ((int)(System.currentTimeMillis() / 1000)) - quizReport.getTimeStart();

        int secondUsed = timeUsed % 60;
        int minuteUsed = (timeUsed - secondUsed) / 60;
        System.out.println(secondUsed);
        System.out.println(minuteUsed);
        System.out.println(quiz.getDuration());
        quizReport.setDurationLeft(quiz.getDuration() - timeUsed);

        int counter = 0;
        System.out.println(json.get("json"));

        List<Map<String, Object>> listMap = (List<Map<String, Object>>) json.get("json");

        for (Map<String, Object> jsonReport : listMap) {
            QuestionReport questionReport = createQuestionReport(idQuiz, jsonReport, token,
                    quizReport.getQuizReportId());
            questionReport.setQuestionReportOwner(quizReport);
            quizReport.getQuestionReportList().add(questionReport);

            questionReportRepository.save(questionReport);
            
            if (questionReport.isTrue()) {
                counter++;
            }
            
        }

        quizReport.setGrade(counter);

        quizReportRepository.save(quizReport);

        return quizReport;

    }

    @Override
    public QuestionReport createQuestionReport(
            int idQuiz, Map<String, Object> jsonReport, String token,
            QuizReportId idQuizReport)
            throws JsonProcessingException {
        String questionString = jsonReport.get("questionString").toString();
        String numberString = jsonReport.get("number").toString();
        int number = Integer.parseInt(numberString.replaceAll("\\s+",""));
        String userAnswer = jsonReport.get("answer").toString();

        String questionJson = restService.getPostsPlainjson(
                String.format(
                        "https://quizku-quiz.herokuapp.com/question/%s/%s", idQuiz, number), token);
//        String questionJson = restService.getPostsPlainjson(
//                String.format(
//                        "http://localhost:8082/question/%s/%s", idQuiz, number), token);

        ObjectMapper objectMapper = new ObjectMapper();
        Question question = objectMapper.readValue(questionJson, Question.class);
        String rightAnswer = question.getAnswer();
        boolean isTrue = userAnswer.equalsIgnoreCase(rightAnswer);

        QuestionReport questionReport = new QuestionReport(
                questionString, rightAnswer, userAnswer, isTrue);
        createQuestionReport(idQuizReport, number, questionReport);
        return questionReport;
    }

    @Override
    public QuestionReport createQuestionReport(
            QuizReportId idQuizReport, int number, QuestionReport questionReport) {
        
        QuestionReportId questionReportId = new QuestionReportId(idQuizReport, number);
        questionReport.setQuestionReportId(questionReportId);
        return questionReport;
        
    }

    @Override
    public Iterable<QuizReport> getAllQuizReport(){
        return quizReportRepository.findAll();
    }

    @Override
    public void deleteQuizReportById(int quizId){
        quizReportRepository.deleteQuizReportsByQuizReportIdQuizId(quizId);
    }

    @Override
    public QuizReport createQuizReport(int idQuiz, String username) {

        QuizReportId quizReportId = new QuizReportId(idQuiz, username);
        QuizReport quizReport = new QuizReport();
        quizReport.setQuizReportId(quizReportId);
        return quizReport;

    }

    @Override
    public Quiz random(int idQuiz, Map<String, Object> json)
            throws JsonProcessingException {
        String token = json.get("token").toString();

        String quizJson = restService.getPostsPlainjson(
                String.format("https://quizku-quiz.herokuapp.com/quiz/%s", idQuiz), token);
//        String quizJson = restService.getPostsPlainjson(
//                String.format("http://localhost:8082/quiz/%s", idQuiz), token);
        ObjectMapper objectMapper = new ObjectMapper();
        Quiz quiz = objectMapper.readValue(quizJson, Quiz.class);
        for (Question question : quiz.getQuestionList()) {
            Collections.shuffle(question.getOption());
        }
        Collections.shuffle(quiz.getQuestionList());
        return quiz;
    }
}
