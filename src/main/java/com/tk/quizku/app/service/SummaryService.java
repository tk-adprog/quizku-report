package com.tk.quizku.app.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.tk.quizku.app.model.QuestionReport;
import java.util.List;
import java.util.Map;

public interface SummaryService {

    int getGradeQuiz(int quizId, Map<String, Object> json) throws JsonProcessingException;

    List<QuestionReport> getQuestionReportList(
            int quizId, Map<String, Object> json)
            throws JsonProcessingException;
}