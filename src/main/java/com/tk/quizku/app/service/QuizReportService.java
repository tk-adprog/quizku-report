package com.tk.quizku.app.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.tk.quizku.app.model.QuestionReport;
import com.tk.quizku.app.model.Quiz;
import com.tk.quizku.app.model.QuizReport;
import com.tk.quizku.app.model.QuizReportId;

import java.util.*;

public interface QuizReportService {

    public QuizReport submit(int idQuiz, Map<String, Object> json) throws JsonProcessingException;

    QuestionReport createQuestionReport(
            int idQuiz, Map<String, Object> jsonReport, String token,
            QuizReportId idQuizReport)
            throws JsonProcessingException;

    QuestionReport createQuestionReport(
            QuizReportId idQuizReport, int number, QuestionReport questionReport);

    Iterable<QuizReport> getAllQuizReport();

    void deleteQuizReportById(int quizId);

    public QuizReport createQuizReport(int idQuiz, String username);

    public Quiz random(int idQuiz, Map<String, Object> json) throws JsonProcessingException;

}
