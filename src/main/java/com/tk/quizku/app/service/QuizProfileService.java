package com.tk.quizku.app.service;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.tk.quizku.app.model.QuizReport;
import java.util.List;

public interface QuizProfileService {

    List<QuizReport> getSortedQuiz(int idQuiz);

    int getAverageScore(int idQuiz);

    int getTotalAttempt(int idQuiz);

    int getAverageTime(int idQuiz, String token) throws JsonProcessingException;

    int updateStatus(int idQuiz, String owner, String token) throws JsonProcessingException;

}
