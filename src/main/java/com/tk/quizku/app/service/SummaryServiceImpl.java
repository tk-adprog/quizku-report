package com.tk.quizku.app.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.tk.quizku.app.core.util.RestService;
import com.tk.quizku.app.model.QuestionReport;
import com.tk.quizku.app.model.QuizReport;
import com.tk.quizku.app.repository.QuizReportRepository;
import com.tk.quizku.auth.config.JwtTokenUtil;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SummaryServiceImpl implements SummaryService {

    @Autowired
    private QuizReportRepository quizReportRepository;

    @Autowired
    RestService restService;

    @Autowired
    JwtTokenUtil jwtTokenUtil;

    @Override
    public int getGradeQuiz(int quizId, Map<String, Object> json) throws  JsonProcessingException {
        String token = json.get("token").toString();
        String username = jwtTokenUtil.getUsernameFromToken(token);
        QuizReport report = quizReportRepository
                .findByQuizReportIdQuizIdAndQuizReportIdOwner(quizId, username);
        int gradeNya = report.getGrade();
        return gradeNya;
    }

    @Override
    public List<QuestionReport> getQuestionReportList(
            int quizId, Map<String, Object> json)
            throws JsonProcessingException {
        String token = json.get("token").toString();
        String username = jwtTokenUtil.getUsernameFromToken(token);
        return quizReportRepository
                .findByQuizReportIdQuizIdAndQuizReportIdOwner(
                        quizId, username)
                .getQuestionReportList();
    }
}