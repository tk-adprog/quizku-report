package com.tk.quizku.app.model;

import java.io.Serializable;
import javax.persistence.Embeddable;
import lombok.Data;
import lombok.NoArgsConstructor;

@Embeddable
@Data
@NoArgsConstructor
public class QuestionReportId implements Serializable {
    private QuizReportId quizReportId;
    private int number;

    public QuestionReportId(QuizReportId quizReportId, int number) {
        this.quizReportId = quizReportId;
        this.number = number;
    }
}