package com.tk.quizku.app.model;

import java.io.Serializable;
import javax.persistence.Embeddable;
import lombok.Data;
import lombok.NoArgsConstructor;

@Embeddable
@Data
@NoArgsConstructor
public class QuizReportId implements Serializable {
    private int quizId;
    private String owner;

    public QuizReportId(int quizId, String owner) {
        this.quizId = quizId;
        this.owner = owner;
    }
}
