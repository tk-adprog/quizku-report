package com.tk.quizku.app.model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "quiz_report")
@Data
@NoArgsConstructor
public class QuizReport implements Comparable<QuizReport> {

    @EmbeddedId
    private QuizReportId quizReportId;

    @OneToMany(mappedBy = "questionReportOwner", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private List<QuestionReport> questionReportList = new ArrayList<QuestionReport>();

    @Column(name = "grade")
    private int grade;

    @Column(name = "timeStart")
    private Integer timeStart = 0;

    @Column(name = "status")
    private Integer status = 0;

    @Column(name = "durationLeft")
    private Integer durationLeft;

    @Override
    public String toString() {
        return "QuizReport{"
                + "quizReportId="
                + quizReportId
                + ", questionReportList=" + questionReportList
                + ", grade="
                + grade
                + ", quizReportOwner=" + quizReportId
                + ",status=" + status
                + ",timeStart=" + timeStart
                + ",durationLeft=" + durationLeft
                + '}';
    }

    @Override
    public int compareTo(QuizReport otherQuiz) {
        return Integer.compare(getGrade(), otherQuiz.getGrade());
    }
}
