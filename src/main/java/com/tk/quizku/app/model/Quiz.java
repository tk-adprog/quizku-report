package com.tk.quizku.app.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "quiz")
@Data
@NoArgsConstructor
public class Quiz {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idQuiz", updatable = false, nullable = false)
    private int idQuiz;

    @Column(name = "title")
    private String title;

    @Column(name = "imageUrl")
    private String imageUrl;

    @Column(name = "duration")
    private int duration;

    @OneToMany(mappedBy = "questionOwner", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private List<Question> questionList = new ArrayList<>();

    @JoinColumn(name = "username")
    private String quizOwner;

    @Column(name = "tag")
    @ElementCollection
    private List<String> tagList = new ArrayList<String>();

    @Column(name = "description")
    private String description;


    public Quiz(String title, String imageUrl, int duration,
                List<String> tagList, String description, String user) {
        this.title = title;
        this.imageUrl = imageUrl;
        this.duration = duration;
        this.tagList = tagList;
        this.description = description;
        this.quizOwner = user;
    }
}
