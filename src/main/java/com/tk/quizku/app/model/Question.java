package com.tk.quizku.app.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name = "question")
@Data
@NoArgsConstructor
public class Question {

    @EmbeddedId
    private QuestionId questionId;

    @Column(name = "question")
    private String question;

    @Column(name = "answer")
    private String answer;

    @Column(name = "option")
    @ElementCollection
    private List<String> option = new ArrayList<>();

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "idQuiz")
    private Quiz questionOwner;

    public Question(String question, String answer, List<String> option) {
        this.question = question;
        this.answer = answer;
        this.option = option;
    }

    @Override
    public String toString() {
        return "QuestionReport{"
                + "question=" + question
                + ", answer=" + answer
                + ", option='" + option
                + '}';
    }

}

