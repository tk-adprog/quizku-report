package com.tk.quizku.app.core.factory;

import com.tk.quizku.app.model.EssayQuestion;
import com.tk.quizku.app.model.MultipleChoice;
import com.tk.quizku.app.model.Question;
import java.util.List;

public class QuestionFactory {
    public Question createQuestion(String type, String question, List<String> options) {
        if (type.equalsIgnoreCase("choice")) {
            return new MultipleChoice(question, options);
        } else if (type.equalsIgnoreCase("essay")) {
            return new EssayQuestion(question, options.get(0));
        }
        return null;
    }

}
