package com.tk.quizku.app.repository;


import com.tk.quizku.app.model.UserQuiz;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserQuizRepository extends JpaRepository<UserQuiz, String> {
    UserQuiz findByUsername(String username);
}
