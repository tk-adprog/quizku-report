package com.tk.quizku.app.repository;

import com.tk.quizku.app.model.QuizReport;
import com.tk.quizku.app.model.QuizReportId;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuizReportRepository extends JpaRepository<QuizReport, QuizReportId> {
    QuizReport findByQuizReportIdQuizIdAndQuizReportIdOwner(int quizId, String owner);

    List<QuizReport> findByQuizReportIdQuizId(int quizId);

    List<QuizReport> findByQuizReportIdOwner(String owner);

    void deleteQuizReportsByQuizReportIdQuizId(int quizId);
}

