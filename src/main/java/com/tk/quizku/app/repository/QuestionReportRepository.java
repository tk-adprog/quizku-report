package com.tk.quizku.app.repository;

import com.tk.quizku.app.model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionReportRepository extends JpaRepository<QuestionReport, QuestionReportId> {
    Question findByQuestionReportIdQuizReportIdAndQuestionReportIdNumber(QuizReportId quizReportId, int number);
}
