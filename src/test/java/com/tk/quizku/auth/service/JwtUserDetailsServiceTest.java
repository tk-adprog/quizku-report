package com.tk.quizku.auth.service;

import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

import com.tk.quizku.app.model.UserQuiz;
import com.tk.quizku.app.repository.UserQuizRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

@ExtendWith(MockitoExtension.class)
public class JwtUserDetailsServiceTest {

    @InjectMocks
    JwtUserDetailsService jwtUserDetailsService;

    @Mock
    UserQuizRepository userQuizRepository;

    @Mock
    PasswordEncoder bcryptEncoder;

    private UserQuiz userQuiz;

    private final String wrongUsername = "jason";
    private final String userQuizUsername = "ian";
    private final String userQuizPass = "password";
    private final String userQuizPassEnc = "password";

    @BeforeEach
    public void setUp() {
        userQuiz = new UserQuiz(
                userQuizUsername,
                "Ian Andersen Ng",
                "ian@mail.com",
                null,
                "blabla.jpg",
                userQuizPass
        );
        System.out.println(userQuiz);

    }

    @Test
    public void testLoadUserByUsernameIsSuccessful() {
        when(userQuizRepository.findByUsername(userQuizUsername)).thenReturn(userQuiz);
        when(bcryptEncoder.encode(userQuizPass)).thenReturn(userQuizPassEnc);

        UserDetails result = jwtUserDetailsService.loadUserByUsername(userQuiz.getUsername());

        Assertions.assertEquals(result.getUsername(), userQuizUsername);
        Assertions.assertEquals(result.getPassword(), userQuizPass);
        System.out.println(userQuiz.getUsername());
    }

    @Test
    public void testLoadByNonExistentUsernameFails() {
        lenient().when(userQuizRepository.findByUsername(wrongUsername)).thenReturn(null);

        Assertions.assertThrows(UsernameNotFoundException.class, () -> {
            jwtUserDetailsService.loadUserByUsername(wrongUsername);
        });
    }

}


