package com.tk.quizku.auth.config;

import java.io.IOException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

@ExtendWith(MockitoExtension.class)
public class JwtAuthenticationEntryPointTest {

    @InjectMocks
    JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

    @Test
    public void testCommence() throws IOException {
        jwtAuthenticationEntryPoint.commence(new MockHttpServletRequest(),
                new MockHttpServletResponse(), null);
    }


}
