package com.tk.quizku.app.service;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tk.quizku.app.core.util.RestService;
import com.tk.quizku.app.model.Quiz;
import com.tk.quizku.app.model.QuizReport;
import com.tk.quizku.app.model.QuizReportId;
import com.tk.quizku.app.model.UserQuiz;
import com.tk.quizku.app.repository.QuizReportRepository;
import com.tk.quizku.auth.config.JwtAuthenticationEntryPoint;
import com.tk.quizku.auth.config.JwtTokenUtil;
import com.tk.quizku.auth.service.JwtUserDetailsService;
import java.util.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

@ExtendWith(MockitoExtension.class)
public class QuizProfileServiceImplTest {

    @Mock
    private QuizReportRepository quizReportRepository;

    @Mock
    private RestService restService;

    @Mock
    private JwtUserDetailsService jwtUserDetailsService;

    @Mock
    private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

    @Mock
    private JwtTokenUtil jwtTokenUtil;


    @InjectMocks
    private QuizProfileServiceImpl quizService;

    private UserQuiz userQuiz;
    private QuizReportId quizReportId;
    private QuizReport quizReport;
    private Quiz quiz;
    private Map<String, Object> mapJson;
    private String userQuizToken;
    private UserDetails userQuizUserDetail;

    @BeforeEach
    public void setup() {
        quiz = new Quiz("Dummy Quiz", "#", 999, new ArrayList<>(), "Test", "DummyUser");
        Date tanggal =  (new Date(2001, 3, 3));
        userQuiz = new UserQuiz("ian.andersen",
                "Ian Andersen Ng",
                "ian@gmail.com",
                tanggal,
                "raja.com",
                "password");
        quizReportId = new QuizReportId(quiz.getIdQuiz(), userQuiz.getUsername());
        quizReport = new QuizReport();
        quizReport.setQuizReportId(quizReportId);
        quizReport.setGrade(90);
        quizReport.setStatus(1);
        quizReport.setDurationLeft(2);

        userQuizUserDetail = new User("ian.andersen","password",new LinkedList<>());
        userQuizToken = jwtTokenUtil.generateToken(userQuizUserDetail);

        mapJson = new HashMap<String, Object>();
        mapJson.put("token", userQuizToken);



    }

    @Test
    public void testServiceGetSortedQuiz() {
        lenient().when(quizService
                .getSortedQuiz(quiz.getIdQuiz()))
                .thenReturn(Arrays.asList(quizReport));
        Iterable<QuizReport> listReportResult = quizService.getSortedQuiz(quiz.getIdQuiz());
        assertNotEquals(null, listReportResult);

    }

    @Test
    public void testServiceGetAverageScore() {

        int averageScore = quizService.getAverageScore(quiz.getIdQuiz());
        assertNotEquals((Integer) null, averageScore);

        List<QuizReport> quizReports = Arrays.asList(quizReport);
        lenient().when(quizReportRepository
                .findByQuizReportIdQuizId(quiz.getIdQuiz()))
                .thenReturn(quizReports);
        averageScore = quizService.getAverageScore(quiz.getIdQuiz());
        assertNotEquals((Integer) null, averageScore);


    }

    @Test
    public void testServiceGetAverageTime() throws JsonProcessingException {
        lenient().when(restService.getPostsPlainjson(any(), any())).thenReturn(mapToJson(quiz));
        int averageTime = quizService.getAverageTime(quiz.getIdQuiz(), userQuizToken);
        assertNotEquals((Integer) null, averageTime);

        List<QuizReport> quizReports = Arrays.asList(quizReport);
        lenient().when(quizReportRepository
                .findByQuizReportIdQuizId(quiz.getIdQuiz()))
                .thenReturn(quizReports);
        averageTime = quizService.getAverageTime(quiz.getIdQuiz(), userQuizToken);
        assertNotEquals((Integer) null, averageTime);
    }

    @Test
    public void testServiceGetTotalAttempt() {
        int totalAttempt = quizService.getTotalAttempt(quiz.getIdQuiz());
        assertNotEquals((Integer) null, totalAttempt);
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }


    @Test
    public void testUpdateStatus() throws JsonProcessingException {
        lenient().when(restService.getPostsPlainjson(any(), any())).thenReturn(mapToJson(quiz));
        lenient().when(quizReportRepository
                .findByQuizReportIdQuizIdAndQuizReportIdOwner(quizReport
                        .getQuizReportId().getQuizId(),quizReport
                        .getQuizReportId().getOwner()))
                .thenReturn(quizReport);
        int statusNow = quizService
                .updateStatus(quiz.getIdQuiz(),
                        quiz.getQuizOwner(), "dummyToken");
        assertNotEquals((Integer) null, statusNow);
    }
}


