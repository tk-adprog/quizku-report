package com.tk.quizku.app.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tk.quizku.app.core.util.RestService;
import com.tk.quizku.app.model.*;
import com.tk.quizku.app.repository.QuestionReportRepository;
import com.tk.quizku.app.repository.QuizReportRepository;
import com.tk.quizku.auth.config.JwtTokenUtil;
import java.util.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

@ExtendWith(MockitoExtension.class)
class QuizReportServiceImplTest {

    @InjectMocks
    QuizReportServiceImpl quizReportServiceImpl;

    @Autowired
    JwtTokenUtil jwtTokenUtil;

    @Mock
    QuizReportRepository quizReportRepository;

    @Mock
    QuestionReportRepository questionReportRepository;

    @Mock
    RestService restService;

    private QuizReport quizReport;
    private Quiz quiz;
    private QuestionReport questionReport;
    private Question question;
    private QuestionId questionId;
    private Map<String, Object> jsonReport;
    private Map<String, Object> jsonReport2;
    private Quiz quizWithOption;

    private Map<String, Object> mapJson;

    private String userQuizToken;
    private UserDetails userQuizUserDetail;

    private String token;

    @BeforeEach
    public void setUp() {
        quiz = new Quiz();
        QuizReportId quizReportId = new QuizReportId();
        quizReportId.setOwner("ravi");
        quizReport = new QuizReport();
        quizReport.setQuizReportId(quizReportId);

        questionId = new QuestionId();

        questionReport = new QuestionReport();
        question = new Question();
        question.setAnswer("benar");
        question.setQuestionId(questionId);

        jsonReport = new HashMap<>();
        jsonReport.put("questionString", "question");
        jsonReport.put("number", 1);
        jsonReport.put("answer", "benar");
        List<Map<String, Object>> jsonReportList = new ArrayList<>();
        jsonReportList.add(jsonReport);

        jsonReport2 = new HashMap<>();
        jsonReport2.put("questionString", "question?");
        jsonReport2.put("number", 1);
        jsonReport2.put("answer", "benar");
        List<Map<String, Object>> jsonReportList2 = new ArrayList<>();
        jsonReportList2.add(jsonReport2);

        Question questionWithOption = new Question();
        List<String> options = new ArrayList<>();
        options.add("jawaban");
        questionWithOption.setOption(options);
        quizWithOption = new Quiz();
        quizWithOption.getQuestionList().add(questionWithOption);

        token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJyYXZpIiwiZXhwIjoxNjIyMjA"
                 + "3MjAxLCJpYXQiOjE2MjIxODkyMDF9.Xhaje"
                 + "DvM7erK4FPepJ2WVuhIZcna6ZjFZ9RNvJbIzF5GPxjdyD3B4Jngzko8_tqP3hOU2T_"
                 + "6OgytSUsExLwzww";

        mapJson = new HashMap<>();
        mapJson.put("json", jsonReportList2);
        mapJson.put("username", "ravi");
        MockitoAnnotations.openMocks(this);
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    public void testSubmit() throws JsonProcessingException {
        userQuizUserDetail = new User("ravi","password",new LinkedList<>());
        JwtTokenUtil util = new JwtTokenUtil();
        userQuizToken = util.generateToken(userQuizUserDetail);
        mapJson.put("token", userQuizToken);
        System.out.println(quiz.getQuestionList());
        System.out.println(mapToJson(quiz));
        lenient().when(restService.getPostsPlainjson(eq("https://quizku-quiz.herokuapp.com/question/0/1"), any())).thenReturn(mapToJson(question));

        lenient().when(restService.getPostsPlainjson(eq("http://quizku-quiz.herokuapp.com/quiz/0"), any())).thenReturn(mapToJson(quiz));
        //lenient().when(quizReportServiceImpl.createQuestionReport(quiz.getIdQuiz(), jsonReport, token, quizReport.getQuizReportId())).thenReturn(questionReport);

        when(quizReportRepository.findByQuizReportIdQuizIdAndQuizReportIdOwner(
                quiz.getIdQuiz(), "ravi")).thenReturn(quizReport);

        assertEquals(quizReportServiceImpl.submit(
                quiz.getIdQuiz(), mapJson), quizReport);
    }

    @Test
    public void testCreateQuestionReport() throws JsonProcessingException{
        userQuizUserDetail = new User("ravi","password",new LinkedList<>());
        JwtTokenUtil util = new JwtTokenUtil();
        userQuizToken = util.generateToken(userQuizUserDetail);
        mapJson.put("token", userQuizToken);
        lenient().when(restService.getPostsPlainjson(any(), any())).thenReturn(mapToJson(question));

        QuizReport quizReport1 = quizReportServiceImpl.createQuizReport(quiz.getIdQuiz(), "ravi");
        QuestionReport questionReport1 = quizReportServiceImpl.createQuestionReport(
                quiz.getIdQuiz(), jsonReport, token, quizReport1.getQuizReportId());

        assertTrue(questionReport1.isTrue());

//        when(quizReportRepository.save(quizReport1)).thenReturn(quizReport);
//        when(questionReportRepository.save(
//        questionReport1)).thenReturn(questionReport);

        QuestionReport questionReport2 = quizReportServiceImpl.createQuestionReport(
                quiz.getIdQuiz(), jsonReport2, token, quizReport.getQuizReportId());

        assertTrue(questionReport2.isTrue());
        question.setAnswer("salah");
    }


    @Test
    public void testRandom() throws JsonProcessingException {
        userQuizUserDetail = new User("ravi","password",new LinkedList<>());
        JwtTokenUtil util = new JwtTokenUtil();
        userQuizToken = util.generateToken(userQuizUserDetail);
        mapJson.put("token", userQuizToken);

        when(restService.getPostsPlainjson(any(), any())).thenReturn(mapToJson(quizWithOption));
        assertEquals(quizReportServiceImpl.random(quiz.getIdQuiz(), mapJson), quizWithOption);
    }

    @Test
    public void testGetAll(){
        quizReportServiceImpl.getAllQuizReport();
    }

    @Test
    public void testDeleteQuizById() {
        quizReportServiceImpl.deleteQuizReportById(2);
    }
}