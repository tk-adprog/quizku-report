package com.tk.quizku.app.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.tk.quizku.app.core.util.RestService;
import com.tk.quizku.app.model.QuestionReport;
import com.tk.quizku.app.model.Quiz;
import com.tk.quizku.app.model.QuizReport;
import com.tk.quizku.app.model.QuizReportId;
import com.tk.quizku.app.model.UserQuiz;
import com.tk.quizku.app.repository.QuizReportRepository;
import com.tk.quizku.auth.config.JwtAuthenticationEntryPoint;
import com.tk.quizku.auth.config.JwtTokenUtil;
import com.tk.quizku.auth.service.JwtUserDetailsService;
import java.util.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UserDetails;

@ExtendWith(MockitoExtension.class)
public class SummaryServiceImplTest {

    @Mock
    private QuizReportRepository quizReportRepository;

    @Mock
    private RestService restService;

    @Mock
    private JwtUserDetailsService jwtUserDetailsService;

    @Mock
    private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

    @Mock
    private JwtTokenUtil jwtTokenUtil;

    @InjectMocks
    private SummaryServiceImpl summaryService;

    private UserQuiz userQuiz;
    private QuizReportId quizReportId;
    private UserQuiz userBaru;
    private Quiz quizBaru;
    private String userQuizToken;
    private QuizReport quizReport;
    private Map<String, Object> json;
    private UserDetails userQuizUserDetail;
    private String token;
    private String username;

    @BeforeEach
    public void setUp() {
        quizBaru = new Quiz("quizTitle", "http://", 30, new ArrayList<>(), "desctest", null);
        Date tanggal =  (new Date(2001, 3, 3));
        userQuiz = new UserQuiz("ian.andersen",
                "Ian Andersen Ng",
                "ian@gmail.com",
                tanggal,
                "raja.com",
                "password"
        );
        quizReportId = new QuizReportId(quizBaru.getIdQuiz(), userQuiz.getUsername());
        quizReport = new QuizReport();
        quizReport.setQuestionReportList(new ArrayList<QuestionReport>());
        quizReport.setQuizReportId(quizReportId);
        quizReport.setGrade(100);

        json = new HashMap<String,Object>();
        json.put("token", "token");
        System.out.print(json.get("token"));
        token = json.get("token").toString();
        username = jwtTokenUtil.getUsernameFromToken(token);
    }

    @Test
    public void testGetGrade() throws JsonProcessingException {
        when(quizReportRepository
                .findByQuizReportIdQuizIdAndQuizReportIdOwner(anyInt(), anyString()))
                .thenReturn(quizReport);
        when(jwtTokenUtil.getUsernameFromToken(anyString())).thenReturn("username");
        int gradeNya = summaryService.getGradeQuiz(1, json);
        System.out.print(gradeNya);
        System.out.println(quizReport.getGrade());
        assertEquals(quizReport.getGrade(), gradeNya);
        assertNotEquals((Integer) null, quizReport.getGrade());
    }

    @Test
    public void testGetQuestionReportList() throws JsonProcessingException {
        when(quizReportRepository
                .findByQuizReportIdQuizIdAndQuizReportIdOwner(anyInt(), anyString()))
                .thenReturn(quizReport);
        when(jwtTokenUtil.getUsernameFromToken(anyString())).thenReturn("username");
        List<QuestionReport> listNya = summaryService
                .getQuestionReportList(quizBaru.getIdQuiz(),json);
        assertEquals(quizReport.getQuestionReportList(),listNya);
    }
}

