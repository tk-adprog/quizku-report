package com.tk.quizku.app.model;

import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class QuizReportTest {

    QuizReport quizReport;
    QuizReport quizReport2;

    @BeforeEach
    public void setUp() {

        quizReport = new QuizReport();
        quizReport.setGrade(1);

        quizReport2 = new QuizReport();
        quizReport2.setGrade(2);
    }

    @Test
    public void testQuestionIdToString() {
        quizReport.toString();
    }

    @Test
    public void testCompareTo() {
        assertNotEquals(2, quizReport.compareTo(quizReport2));
    }
}