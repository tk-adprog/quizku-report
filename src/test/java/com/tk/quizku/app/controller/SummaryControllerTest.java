package com.tk.quizku.app.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tk.quizku.app.model.*;
import com.tk.quizku.app.service.SummaryServiceImpl;
import com.tk.quizku.auth.config.JwtAuthenticationEntryPoint;
import com.tk.quizku.auth.config.JwtTokenUtil;
import com.tk.quizku.auth.service.JwtUserDetailsService;
import java.util.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(controllers = SummaryController.class)
public class SummaryControllerTest {

    private final Map<String, Object> json = new HashMap<>();
    @Autowired
    private MockMvc mvc;

    @MockBean
    private SummaryServiceImpl summaryService;

    @MockBean
    private JwtUserDetailsService jwtUserDetailsService;

    @MockBean
    private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

    @MockBean
    private JwtTokenUtil jwtTokenUtil;

    private UserQuiz userQuiz;
    private String userQuizToken;
    private UserDetails userQuizUserDetail;

    private QuizReportId quizReportId;
    private UserQuiz userBaru;
    private Quiz quizBaru;
    private QuizReport quizReport;

    @BeforeEach
    public void setUp() {
        Date tanggal =  (new Date(2001, 3, 3));

        userQuiz = new UserQuiz("ian.andersen",
                "Ian Andersen Ng",
                "ian@gmail.com",
                tanggal,
                "raja.com",
                "password"
        );

        userQuizUserDetail = new User("ian.andersen","password",new LinkedList<>());
        userQuizToken = jwtTokenUtil.generateToken(userQuizUserDetail);

        json.put("token", userQuizToken);

        quizBaru = new Quiz("quizTitle", "http://", 30, new ArrayList<>(), "desctest", null);
        userBaru = new UserQuiz();
        quizBaru.setIdQuiz(1);
        userBaru.setUsername("dummy.test");
        userBaru.setFullname("dummy test");
        userBaru.setEmail("dummy@gmail.com");
        quizReportId = new QuizReportId(quizBaru.getIdQuiz(), userBaru.getUsername());
        quizReport = new QuizReport();
        quizReport.setQuizReportId(quizReportId);
        quizReport.setGrade(100);
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    @WithMockUser(username = "ian.andersen")
    public void testSummaryControllerPage() throws Exception {
        mvc.perform(get("/summary/1")
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + userQuizToken))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "ian.andersen")
    public void testSummaryControllerGetGrade() throws Exception {
        when(summaryService
                .getGradeQuiz(quizBaru.getIdQuiz(), json))
                .thenReturn(quizReport.getGrade());
        System.out.println("gradeNya : " + summaryService.getGradeQuiz(quizBaru.getIdQuiz(), json));
        assertEquals(quizReport.getGrade(),100);
        mvc.perform(post("/summary/1/gradeNya")
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + userQuizToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapToJson(json)))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON));;
    }

    @Test
    @WithMockUser(username = "ian.andersen")
    public void testSummaryControllerGetListOfQuestionReport() throws Exception {
        List<QuestionReport> questionReportNya = summaryService
                .getQuestionReportList(
                        quizBaru.getIdQuiz(), json);
        when(summaryService.getQuestionReportList(quizBaru.getIdQuiz(), json))
                .thenReturn(quizReport.getQuestionReportList());
        assertEquals(quizReport
                .getQuestionReportList(),questionReportNya);
        mvc.perform(post("/summary/1/questionReport")
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + userQuizToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapToJson(json)))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }
}