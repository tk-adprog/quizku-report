package com.tk.quizku.app.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.tk.quizku.app.core.util.RestService;
import com.tk.quizku.app.model.Quiz;
import com.tk.quizku.app.model.QuizReport;
import com.tk.quizku.app.model.QuizReportId;
import com.tk.quizku.app.model.UserQuiz;
import com.tk.quizku.app.repository.QuizReportRepository;
import com.tk.quizku.auth.config.JwtAuthenticationEntryPoint;
import com.tk.quizku.auth.config.JwtTokenUtil;
import com.tk.quizku.auth.service.JwtUserDetailsService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.text.ParseException;
import java.util.*;

import static com.tk.quizku.app.controller.JawabControllerTest.asJsonString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.lenient;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = QuizReportController.class)
public class QuizReportControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    JwtTokenUtil jwtTokenUtil;

    @MockBean
    QuizReportRepository quizReportRepository;

    @MockBean
    RestService restService;

    @MockBean
    JwtUserDetailsService jwtUserDetailsService;

    @MockBean
    JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

    private UserQuiz userQuiz;
    private String userQuizToken;
    private UserDetails userQuizUserDetail;

    private Quiz quiz;
    private QuizReport quizReport;
    private QuizReportId quizReportId;
    private UserQuiz user;
    private final Map<String, Object> json = new HashMap<>();
    List<QuizReport> reportList;

    @BeforeEach
    public void setUp() throws ParseException {
        Date tanggal =  (new Date(2001, 3, 3));

        userQuiz = new UserQuiz("ian.andersen",
                "Ian Andersen Ng",
                "ian@gmail.com",
                tanggal,
                "raja.com",
                "password"
        );

        userQuizUserDetail = new User("ian.andersen","password",new LinkedList<>());
        userQuizToken = jwtTokenUtil.generateToken(userQuizUserDetail);

        //soon to be added object needed for test
        List<String> tagList = new ArrayList<>();
        tagList.add("a");
        tagList.add("b");
        quiz = new Quiz("Dummy Quiz", "#", 999, tagList, "Test", null);
        user = new UserQuiz();
        user.setUsername("DummyTest");
        user.setFullname("Dummy Test");
        user.setEmail("Dummy@gmail.com");
        quizReportId = new QuizReportId(quiz.getIdQuiz(), user.getUsername());
        quizReport = new QuizReport();
        quizReport.setQuizReportId(quizReportId);
        quizReport.setGrade(90);
        reportList = new ArrayList<>();
        reportList.add(quizReport);
        json.put("token","token");
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    @WithMockUser(username = "ian.andersen")
    public void testControllerGetQuizStatistics() throws Exception {
        lenient().when(quizReportRepository.findByQuizReportIdOwner(any())).thenReturn(reportList);
        lenient().when(restService.getPostsPlainjson(any(), any())).thenReturn(mapToJson(quiz));

        mvc.perform(post("/report/getQuizAnswered")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + "abcdefg")
                .content(asJsonString(json)))
                .andExpect(status().isOk());
    }

}
