package com.tk.quizku.app.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.tk.quizku.app.core.factory.QuestionFactory;
import com.tk.quizku.app.core.util.RestService;
import com.tk.quizku.app.model.*;
import com.tk.quizku.app.repository.QuizReportRepository;
import com.tk.quizku.app.service.QuizReportService;
import com.tk.quizku.auth.config.JwtAuthenticationEntryPoint;
import com.tk.quizku.auth.config.JwtTokenUtil;
import com.tk.quizku.auth.service.JwtUserDetailsService;
import java.util.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
public class JawabControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private RestService restService;

    @MockBean
    private QuizReportService quizReportService;

    @MockBean
    private QuizReportRepository quizReportRepository;

    @MockBean
    private JwtUserDetailsService jwtUserDetailsService;

    @MockBean
    private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

    @MockBean
    private JwtTokenUtil jwtTokenUtil;


    private Quiz quiz;
    private QuizReport quizReport;
    private QuizReportId idQuizReport;
    private List<Map<String, Object>> array = new ArrayList<Map<String, Object>>();

    private UserQuiz userQuiz;
    private String userQuizToken;
    private UserDetails userQuizUserDetail;

    private Map<String, Object> mapJson;

    private String token;

    private Map<String,Object> json = new HashMap<>();

    @BeforeEach
    public void setUp() {
        HashMap<String, Object> map1 = new HashMap<>();
        map1.put("number", 1);
        map1.put("tipe", "pilgan");
        map1.put("answer", "A");

        HashMap<String, Object> map2 = new HashMap<>();
        map2.put("number", 2);
        map2.put("tipe", "essay");
        map2.put("answer", "WOWAN");

        array.add(map1);
        array.add(map2);

        List<String> tag = new ArrayList<>();
        tag.add("tag1");
        tag.add("tag2");
        quiz = new Quiz("judul", "url", 2022819666, tag, "ini quiz", null);
        List<String> options = new ArrayList<>();
        options.add("a");
        options.add("b");
        options.add("c");
        options.add("d");
        QuestionFactory questionFactory = new QuestionFactory();
        List<Question> questionsList = new ArrayList<>();
        questionsList.add(questionFactory.createQuestion("choice", "pg?", options));
        options.add("true");
        questionsList.add(questionFactory.createQuestion("essay", "essay?", options));
        quiz.setQuestionList(questionsList);

        idQuizReport = new QuizReportId();
        idQuizReport.setQuizId(quiz.getIdQuiz());

        quizReport = new QuizReport();
        quizReport.setGrade(1);
        quizReport.setQuizReportId(idQuizReport);
        quizReport.setTimeStart(0);
        json.put("token","token");

        Date tanggal =  (new Date(2001, 3, 3));

        userQuiz = new UserQuiz("ian.andersen",
                "Ian Andersen Ng",
                "ian@gmail.com",
                tanggal,
                "raja.com",
                "password"
        );

        idQuizReport.setOwner(userQuiz.getUsername());
        token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJyYXZpIiwiZXhwIjoxNjIyMjA"
                + "3MjAxLCJpYXQiOjE2MjIxODkyMDF9.Xhaje"
                + "DvM7erK4FPepJ2WVuhIZcna6ZjFZ9RNvJbIzF5GPxjdyD3B4Jngzko8_tqP3hOU2T_"
                + "6OgytSUsExLwzww";

        userQuizUserDetail = new User("ian.andersen","password",new LinkedList<>());
        userQuizToken = jwtTokenUtil.generateToken(userQuizUserDetail);

        mapJson = new HashMap<String, Object>();
        mapJson.put("token", "token");
    }

    @Test
    @WithMockUser(username = "ian.andersen")
    public void testGetJawabForm() throws Exception {
        mvc.perform(get("/jawab/1")
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + userQuizToken))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "ian.andersen")
    public void testPostJawabForm() throws Exception {
        when(quizReportService.random(1, mapJson)).thenReturn(quiz);
        mvc.perform(post("/jawab/1").contentType(MediaType.APPLICATION_JSON_VALUE)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + userQuizToken)
                .content(asJsonString(quiz)))
                .andExpect(status().isOk());
    }


    @Test
    @WithMockUser(username = "ian.andersen")
    public void testSubmitQuiz() throws Exception {
        when(quizReportService.submit(1, mapJson)).thenReturn(quizReport);
        when(jwtTokenUtil.getUsernameFromToken(userQuizToken)).thenReturn("ian.andersen");
        mvc.perform(post("/jawab/1/submit").contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(asJsonString(mapJson))
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + userQuizToken))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "ian.andersen")
    public void testGetUserDurationLeft() throws Exception {
        lenient().when(restService.getPostsPlainjson(any(), any())).thenReturn(mapToJson(quiz));
        int quizReportId = quizReport.getQuizReportId().getQuizId();
        String quizReportOwner = quizReport.getQuizReportId().getOwner();
        when(jwtTokenUtil.getUsernameFromToken("token")).thenReturn(userQuiz.getUsername());
        lenient().when(quizReportRepository
                .findByQuizReportIdQuizIdAndQuizReportIdOwner(
                        quizReportId,quizReportOwner))
                .thenReturn(quizReport);
        mvc.perform(post("/jawab/" + quiz.getIdQuiz() + "/durationLeft")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + "abcdefg")
                .content(asJsonString(quiz)).content(asJsonString(json)))
                .andExpect(status().isOk());

        quizReport.setTimeStart(999999999);
        mvc.perform(post("/jawab/" + quiz.getIdQuiz() + "/durationLeft")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + "abcdefg")
                .content(asJsonString(quiz))
                .content(asJsonString(json)))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "ian.andersen")
    public void testGetUserSmallDurationLeft() throws Exception {
        lenient().when(restService.getPostsPlainjson(any(), any())).thenReturn(mapToJson(quiz));
        int quizReportId = quizReport.getQuizReportId().getQuizId();
        String quizReportOwner = quizReport.getQuizReportId().getOwner();
        when(jwtTokenUtil.getUsernameFromToken("token")).thenReturn(userQuiz.getUsername());
        lenient().when(quizReportRepository
                .findByQuizReportIdQuizIdAndQuizReportIdOwner(
                        quizReportId,quizReportOwner))
            .thenReturn(quizReport);

        quizReport.setTimeStart(999999999);
        quiz.setDuration(30);
        lenient().when(restService.getPostsPlainjson(any(), any())).thenReturn(mapToJson(quiz));
        mvc.perform(post("/jawab/" + quiz.getIdQuiz() + "/durationLeft")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
            .header(HttpHeaders.AUTHORIZATION, "Bearer " + "abcdefg")
                .content(asJsonString(quiz)).content(asJsonString(json)))
            .andExpect(status().isOk());
    }

    public static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    @WithMockUser(username = "ian.andersen")
    public void testControllerGetAllQuizProfile() throws Exception {
        List<QuizReport> list = new ArrayList<>();
        list.add(quizReport);
        when(quizReportService.getAllQuizReport()).thenReturn(list);
        mvc.perform(get(
                "/jawab/all")
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + "abcdefg"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "ian.andersen")
    public void testControllerDeleteQuizProfile() throws Exception {
        List<QuizReport> list = new ArrayList<>();
        list.add(quizReport);
        when(quizReportService.getAllQuizReport()).thenReturn(list);
        mvc.perform(get(
                "/jawab/all/1")
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + "abcdefg"))
                .andExpect(status().isOk());
    }
    @Test
    @WithMockUser(username = "ian.andersen")
    public void testControllerDeleteAllQuizProfile() throws Exception {
        mvc.perform(get(
                "/jawab/self/destruct")
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + "abcdefg"))
                .andExpect(status().is3xxRedirection());
    }


}