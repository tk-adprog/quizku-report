package com.tk.quizku.app.controller;

import static com.tk.quizku.app.controller.JawabControllerTest.asJsonString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.tk.quizku.app.core.util.RestService;
import com.tk.quizku.app.model.Quiz;
import com.tk.quizku.app.model.QuizReport;
import com.tk.quizku.app.model.QuizReportId;
import com.tk.quizku.app.model.UserQuiz;
import com.tk.quizku.app.repository.QuizReportRepository;
import com.tk.quizku.app.service.QuizProfileService;
import com.tk.quizku.auth.config.JwtAuthenticationEntryPoint;
import com.tk.quizku.auth.config.JwtTokenUtil;
import com.tk.quizku.auth.service.JwtUserDetailsService;
import java.text.ParseException;
import java.util.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(controllers = QuizProfileController.class)
public class QuizProfileControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private QuizProfileService quizProfileService;

    @MockBean
    private JwtUserDetailsService jwtUserDetailsService;

    @MockBean
    private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

    @MockBean
    private JwtTokenUtil jwtTokenUtil;

    @MockBean
    private RestService restService;

    @MockBean
    private QuizReportRepository quizReportRepository;

    private UserQuiz userQuiz;
    private String userQuizToken;
    private UserDetails userQuizUserDetail;

    private Quiz quiz;
    private QuizReport quizReport;
    private QuizReportId quizReportId;
    private UserQuiz user;
    private final Map<String, Object> json = new HashMap<>();

    @BeforeEach
    public void setUp() throws ParseException {
        Date tanggal =  (new Date(2001, 3, 3));

        userQuiz = new UserQuiz("ian.andersen",
                "Ian Andersen Ng",
                "ian@gmail.com",
                tanggal,
                "raja.com",
                "password"
        );

        userQuizUserDetail = new User("ian.andersen","password",new LinkedList<>());
        userQuizToken = jwtTokenUtil.generateToken(userQuizUserDetail);

        //soon to be added object needed for test
        List<String> tagList = new ArrayList<>();
        tagList.add("a");
        tagList.add("b");
        quiz = new Quiz("Dummy Quiz", "#", 999, tagList, "Test", null);
        user = new UserQuiz();
        user.setUsername("DummyTest");
        user.setFullname("Dummy Test");
        user.setEmail("Dummy@gmail.com");
        quizReportId = new QuizReportId(quiz.getIdQuiz(), user.getUsername());
        quizReport = new QuizReport();
        quizReport.setQuizReportId(quizReportId);
        quizReport.setGrade(90);
        List<QuizReport> reportList = new ArrayList<>();
        reportList.add(quizReport);
        json.put("token","token");
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    @WithMockUser(username = "ian.andersen")
    public void testControllerGetQuizProfilePage() throws Exception {
        mvc.perform(get("/profile/" + quiz.getIdQuiz())
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + "abcdefg"))
                .andExpect(status().isOk());
    }


    @Test
    @WithMockUser(username = "ian.andersen")
    public void testControllerGetQuizProfile() throws Exception {
        List<QuizReport> sortedList = quizProfileService.getSortedQuiz(quiz.getIdQuiz());
        when(quizProfileService.getSortedQuiz(quiz.getIdQuiz())).thenReturn(sortedList);
        mvc.perform(get(
                "/profile/leaderboard/" + quiz.getIdQuiz())
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + "abcdefg"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "ian.andersen")
    public void testControllerGetQuizStatistics() throws Exception {
        lenient().when(restService.getPostsPlainjson(any(), any())).thenReturn(mapToJson(userQuiz));
        when(jwtTokenUtil.getUsernameFromToken("token")).thenReturn(userQuiz.getUsername());
        mvc.perform(
                post("/profile/statistics/" + quiz.getIdQuiz())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + "abcdefg")
                .content(asJsonString(quiz))
                .content(asJsonString(json)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0]").value(
                        quizProfileService.getAverageScore(quiz.getIdQuiz())))
                .andExpect(jsonPath("$[1]").value(
                        quizProfileService.getTotalAttempt(quiz.getIdQuiz())))
                .andExpect(jsonPath("$[2]").value(
                        quizProfileService.getAverageTime(quiz.getIdQuiz(), userQuizToken)));

    }

    @Test
    @WithMockUser(username = "ian.andersen")
    public void testControllerGetQuizProfileData() throws Exception {
        lenient().when(restService.getPostsPlainjson(any(), any())).thenReturn(mapToJson(quiz));
        when(jwtTokenUtil.getUsernameFromToken("token")).thenReturn(userQuiz.getUsername());
        mvc.perform(post("/profile/" + quiz.getIdQuiz())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + "abcdefg")
                .content(asJsonString(quiz))
                .content(asJsonString(json)))
                .andExpect(status().isOk());
    }

}


