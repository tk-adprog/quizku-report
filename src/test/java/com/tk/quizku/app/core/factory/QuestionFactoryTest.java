package com.tk.quizku.app.core.factory;

import static org.junit.jupiter.api.Assertions.*;

import com.tk.quizku.app.model.EssayQuestion;
import com.tk.quizku.app.model.MultipleChoice;
import com.tk.quizku.app.model.Question;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class QuestionFactoryTest {

    private Class<?> questionFactoryClass;

    @BeforeEach
    public void setup() throws Exception {
        questionFactoryClass = Class.forName("com.tk.quizku.app.core.factory.QuestionFactory");
    }

    @Test
    public void testQuestionFactoryIsAPublicConcrete() {
        int classModifiers = questionFactoryClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertFalse(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testQuestionFactoryCreateEssayQuestion() {
        Question question = new EssayQuestion("pertanyaan?", "jawaban");
        List<String> optionList = new ArrayList<>();
        optionList.add("jawaban");
        QuestionFactory questionFactory = new QuestionFactory();
        Question questionResult = questionFactory.createQuestion("essay",
                "pertanyaan?", optionList);
        assertEquals(question.toString(), questionResult.toString());
    }

    @Test
    public void testQuestionFactoryCreateChoiceQuestion() {
        List<String> optionList = new ArrayList<>();
        optionList.add("a");
        optionList.add("b");
        optionList.add("c");
        optionList.add("d");
        Question question = new MultipleChoice("choice?", optionList);
        QuestionFactory questionFactory = new QuestionFactory();
        Question questionResult = questionFactory.createQuestion("choice",
                "choice?", optionList);
        assertEquals(question.toString(), questionResult.toString());
    }

    @Test
    public void testQuestionFactoryCreateUnknownQuestion() {
        List<String> optionList = new ArrayList<>();
        QuestionFactory questionFactory = new QuestionFactory();
        Question questionResult = questionFactory.createQuestion("unknown",
                "pertanyaan", optionList);
        assertEquals(null, questionResult);
    }
}
