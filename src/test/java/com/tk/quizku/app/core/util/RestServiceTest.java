package com.tk.quizku.app.core.util;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.web.client.RestTemplate;

class RestServiceTest {


    RestTemplate restTemplate;
    RestTemplateBuilder restTemplateBuilder;
    RestService restService;
    String url = "https://www.google.co.id/";

    @BeforeEach
    public void setUp() {
        restTemplate = new RestTemplate();
        restTemplateBuilder = new RestTemplateBuilder();
        restService = new RestService(restTemplateBuilder);
    }

    @Test
    void testGetPostsPlainjson() {
        restService.getPostsPlainjson(url,"token");
    }
}